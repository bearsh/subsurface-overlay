# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="QtLocation plugin for Google maps tile API"
HOMEPAGE="https://github.com/vladest/googlemaps"

if [[ ${PV} == 9999 ]]; then
	GIT_ECLASS="git-r3"
	EGIT_REPO_URI="https://github.com/vladest/googlemaps.git"
else
	SHA_HASH="54a357f9590d9cf011bf1713589f66bad65e00eb"
	SRC_URI="https://github.com/vladest/googlemaps/archive/${SHA_HASH}.tar.gz -> ${P}.tar.gz"
fi

inherit qmake-utils ${GIT_ECLASS}

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

IUSE=""

# googlemaps uses private headers from qtlocation
DEPEND=">=dev-qt/qtcore-5.7
	>=dev-qt/qtlocation-5.7:=
	>=dev-qt/qtnetwork-5.7
	>=dev-qt/qtpositioning-5.7
"
RDEPEND="${DEPEND}"

src_unpack() {
	default
	[[ ${PV} != 9999 ]] && mv googlemaps* ${P} || die "renaming src failed"
}

src_configure() {
	eqmake5
}

src_install() {
	emake INSTALL_ROOT="${D}" install
}

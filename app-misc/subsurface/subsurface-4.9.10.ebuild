# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

if [[ ${PV} == 9999 ]]; then
	GIT_ECLASS="git-r3"
	EGIT_REPO_URI="https://github.com/Subsurface-divelog/subsurface"
	KEYWORDS=""
else
	SRC_URI="https://subsurface-divelog.org/downloads/Subsurface-${PV}.tgz -> ${P}.tar.gz
		https://subsurface-divelog.org/downloads/libdivecomputer-subsurface-branch-${PV}.tgz
	"
	KEYWORDS="~amd64"
fi

PLOCALES="
	bg_BG ca cs da_DK de_CH de_DE el_GR en_GB es_ES et_EE fi_FI fr_FR he hr_HR
	it_IT nb_NO nl_NL pl_PL pt_BR pt_PT ro_RO ru_RU sk_SK sv_SE zh_TW
"

inherit eutils l10n autotools cmake ${GIT_ECLASS}

DESCRIPTION="An open source dive log program"
HOMEPAGE="http://subsurface-divelog.org"
LICENSE="GPL-2"
SLOT="0"
IUSE="doc bluetooth printsupport"

COMMON_DEPEND="
	dev-db/sqlite:3
	dev-libs/grantlee:5
	virtual/libusb:1
	>=dev-libs/libgit2-0.23:=
	dev-libs/libxml2
	dev-libs/libxslt
	dev-libs/libzip
	dev-libs/openssl:0
	bluetooth? (
		net-wireless/bluez
		dev-qt/qtbluetooth:5
	)
	dev-qt/qtconcurrent:5
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtdeclarative:5
	dev-qt/qtlocation:5
	dev-qt/qtnetwork:5
	dev-qt/qtprintsupport:5
	dev-qt/qtpositioning
	dev-qt/qtsvg:5
	printsupport? ( dev-qt/qtwebkit:5[printsupport] )
	dev-qt/qtwidgets:5
	net-libs/libssh2
	net-misc/curl
	sys-libs/zlib
"

RDEPEND="${COMMON_DEPEND}
	>=dev-qt/googlemaps-0.0_pre20171022
"
BDEPEND="doc? ( app-text/asciidoc )
	dev-qt/linguist-tools:5
"
DEPEND="${RDEPEND}"

DOCS="subsurface/README.md"
SSRF_PATCHES=( )
S="${WORKDIR}"
CMAKE_MAKEFILE_GENERATOR=emake
CMAKE_USE_DIR="${S}/${PN}"
BUILD_DIR="${S}/subsurface-build"

src_unpack() {
	if [[ ${PV} == 9999 ]]; then
		git-r3_src_unpack
	else
		unpack ${A}
	fi
}

src_prepare() {
	mv [Ss]ubsurface* "${PN}" || die "renaming subsurface src failed"

	pushd subsurface >/dev/null || die "pushd failed"
	use doc || { sed -i '/Documentation/d' CMakeLists.txt || die "sed failed"; }
	cmake_src_prepare
	[ ${#SSRF_PATCHES[@]} -gt 0 ] && eapply ${SSRF_PATCHES[@]}
	popd >/dev/null || die "popd failed"

	if [[ ${PV} == 9999 ]]; then
		# libdivecomputer is a submodule, move it out of subsurface
		mv subsurface/libdivecomputer libdc || die "move libdivecomputer failed"
	else
		# rename libd{c,computer,...} to libdc
		mv libd* libdc || die "renaming bundled libdivecomputer src failed"
	fi
	pushd libdc >/dev/null || die "pushd failed"
	eautoreconf
	popd >/dev/null || die "popd failed"
}

src_configure() {
	local mycmakeargs

	einfo "configuring bundled libdivecomputer"
	pushd libdc >/dev/null || die "pushd faild"
	econf $(use_with bluetooth bluez) --disable-examples
	popd >/dev/null

	einfo "configuring subsurface"
	mycmakeargs=(
		-DLIBDIVECOMPUTER_INCLUDE_DIR="${S}/libdc/include"
		-DLIBDIVECOMPUTER_LIBRARIES="${S}/libdc/src/.libs/libdivecomputer.a"
		-DLIBGIT2_FROM_PKGCONFIG=ON
		-DLIBGIT2_DYNAMIC=ON
		-DMAKE_TESTS=NO
		-DBTSUPPORT=$(usex bluetooth ON OFF)
		-DNO_DOCS=$(usex doc OFF ON)
		-DNO_USERMANUAL=$(usex doc OFF ON)
		-DFORCE_LIBSSH=1
		-DNO_PRINTING=$(usex printsupport OFF ON)
	)
	cmake_src_configure
}

src_compile() {
	einfo "compile bundled libdivecomputer"
	pushd libdc >/dev/null || die "pushd faild"
	emake
	popd >/dev/null

	einfo "compile subsurface"
	cmake_src_compile
}

rm_trans() {
	rm "${ED}/usr/share/${PN}/translations/${PN}_${1}.qm" || die "rm ${PN}_${1}.qm failed"
}

src_install() {
	cmake_src_install

	l10n_for_each_disabled_locale_do rm_trans

	# this is not a translation but present (no need to die if not present)
	rm "${ED}/usr/share/${PN}/translations/${PN}_source.qm"
}

# Subsurface overlay #

As this overlay is currently not listed in layman's index, add it with the following command as root to the local list of overlays:


```
#!bash

wget -O /etc/layman/overlays/subsurface.xml https://bitbucket.org/bearsh/subsurface-overlay/raw/HEAD/Documentation/overlay.xml
```
